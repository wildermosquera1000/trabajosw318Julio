﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace trabajowilder
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarproducto", ResponseFormat = WebMessageFormat.Json)]
        List<producto> mostrarproductos();


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarproducto/{Id}", ResponseFormat = WebMessageFormat.Json)]
        producto mostrarunproducto(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarcategoria", ResponseFormat = WebMessageFormat.Json)]
        producto mostrarcategoria();
        // TODO: agregue aquí sus operaciones de servicio
    }


    
}
