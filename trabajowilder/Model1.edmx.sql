
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/17/2015 23:58:33
-- Generated from EDMX file: C:\Users\DAVID\Desktop\sw3\trabajowilder\trabajowilder\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [master];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'productoSet'
CREATE TABLE [dbo].[productoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre_producto] nvarchar(max)  NOT NULL,
    [cantidad] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'categoriaSet'
CREATE TABLE [dbo].[categoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [tipo] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'productoSet'
ALTER TABLE [dbo].[productoSet]
ADD CONSTRAINT [PK_productoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'categoriaSet'
ALTER TABLE [dbo].[categoriaSet]
ADD CONSTRAINT [PK_categoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------